const Needle = require('needle');
const FS = require('fs');
const Libs = require('./libs')
const TOKEN = process.env.GITHUB_TOKEN || process.exit();
const REFRESH_DELAY = process.env.REFRESH_DELAY || process.exit();
const baseURL = "https://api.github.com";
const RPP = 100;
const params = {
	json: true,
	headers: {
		Authorization: 'token '+ TOKEN,
		Accept: 'application/vnd.github.v3+json'
	}
};

function getPage(page, repos) {
	return new Promise((resolve, reject) => {
		page = page || 1;
		repos = repos || [];
		console.log(`Fetching page ${page}`);
		let url = `${baseURL}/user/repos?type=all&per_page=${RPP}&page=${page}`;
		Needle('get', url, params).then(response => {
			response = response.body.map(repo => {
				return {
					name: repo.name,
					desc: repo.description,
					createdAt: repo.created_at,
					stars: repo.stargazers_count,
					tags: repo.topic
				}
			});
			// Add the retrieved repos to the list
			repos = repos.concat(response);

			// Check if the length of returned repos is
			// less than the RPP which means the list has
			// ended.
			if (response.length < RPP) {
				console.log(`Complete`);
				resolve(repos);
			} else {
				getPage(++page, repos).then(repos => {
					resolve(repos);
				});
			}
		}).catch(error => {
			console.log(error);
			process.exit();
		});
	});
}

async function start() {
	let repos = await getPage();
	console.log("Github Repo Count:", repos.length);
	FS.writeFileSync("github.json", JSON.stringify(repos));
	delete repos;
	console.log('Refreshed Github count at:', Libs.getHumanDate(new Date()));
	await Libs.sleep(REFRESH_DELAY);
	start().catch(console.error);
}

start().catch(console.error);