const Needle = require('needle');
const FS = require('fs');
const Libs = require('./libs')
const TOKEN = process.env.GITLAB_TOKEN || process.exit();
const REFRESH_DELAY = process.env.REFRESH_DELAY || process.exit();
const baseURL = "https://gitlab.com/api/v4";
const RPP = 100;
const params = {
	json: true,
	headers: {
		'Private-Token': TOKEN
	}
};

function getPage(page, repos) {
	return new Promise((resolve, reject) => {
		page = page || 1;
		repos = repos || [];
		// console.log(`Fetching page ${page}`);
		let url = `${baseURL}/projects?membership=true&simple=true&per_page=${RPP}&page=${page}`;
		Needle('get', url, params).then(response => {
			response = response.body.map(repo => {
				return {
					name: repo.name,
					desc: repo.description,
					createdAt: repo.created_at,
					stars: repo.star_count,
					tags: repo.tag_list
				}
			});
			// Add the retrieved repos to the list
			repos = repos.concat(response);

			// Check if the length of returned repos is
			// less than the RPP which means the list has
			// ended.
			if (response.length < RPP) {
				// console.log(`Complete`);
				resolve(repos);
			} else {
				getPage(++page, repos).then(repos => {
					resolve(repos);
				});
			}
		}).catch(error => {
			console.log(error);
			process.exit();
		});
	});
}


async function start() {
	let repos = await getPage();
	console.log("Gitlab Repo Count:", repos.length);
	FS.writeFileSync("gitlab.json", JSON.stringify(repos));
	delete repos;
	console.log('Refreshed GitLab count at:', Libs.getHumanDate(new Date()));
	await Libs.sleep(REFRESH_DELAY);
	start().catch(console.error);
}

start().catch(console.error);