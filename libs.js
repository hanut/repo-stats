module.exports = {
	/**
	 * Async sleep for the given number of miliseconds
	 * @param  {Number} - time The amount of time in miliseconds
	 * @return {Promise} - A promise that resolves when the sleep is over
	 */
	sleep(time) {
		return new Promise((resolve, reject) => {
			try {
				let timer = setTimeout(() => {
					clearTimeout(timer);
					resolve()
				}, time);

			} catch (error) {
				reject(error);				
			}
		});
	},

	getHumanDate(date) {
		let year = ''+date.getFullYear();
		let month = ''+date.getMonth() + 1;
		let day = ''+date.getDate();
		let hours = ''+date.getHours();
		let minutes = ''+date.getMinutes();
		let seconds = ''+date.getSeconds();

		if (day.length < 2) {
			day = "0" + day;
		}
		if (month.length < 2) {
			month = "0" + month;
		}
		if (hours.length < 2) {
			hours = "0" + hours;
		}
		if (minutes.length < 2) {
		}
		if (seconds.length < 2) {
			seconds = "0" + seconds;
		}

		return `${day}/${month}/${year} at ${hours}:${minutes}:${seconds}`;
	}

};