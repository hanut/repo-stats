# Repo Stats

This project lets you simply and easily read your repo stats from gitlab and github
and output json dumps from the same. All using the power of Node.js

### Notice

The scripts use keys from the environment and as such will fail unless these are found.

If you are on a Unix like system use 
````
export GITLAB_TOKEN="<Your GitLab token goes here>"

or

export GITHUB_TOKEN="<Your Github token goes here>"
````